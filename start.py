from app_name.extensions import db
from flask import Flask

from app_name.views import main


def create_app(config_file='settings.cfg'):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile(config_file)
    db.init_app(app)
    app.register_blueprint(main)
    return app


if __name__ == '__main__':
    app = create_app()
    app.run()
